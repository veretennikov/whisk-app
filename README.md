# ::FOOD:APP

## Install
```
cd <application-path>
yarn
```

## Application
Will not work without proxy!

Start application with proxy
```
yarn dev
```

Start application only
```
yarn start
```

## Proxy
For development only!

Start proxy server
```
export NODE_ENV=development; yarn proxy
```

Start proxy server with debug
```
export NODE_ENV=development; export DEBUG=express-http-proxy; yarn run proxy
```

## TODO:
- write tests
- transfer API endpoints to config
- transfer data normalization from services/api-provider.ts to separate layer, use it in store/thunks.ts
- write error system, use it in services/api-provider.ts
- normalize store: divide it by the business entity (recipes, users, orders ...) and the application data (isLoading, recipeFeed, activeRecipe)
- write more strong grid in layout (use props, layout must share children itself)
- rewrite button-link (must be only link like button)
- remove all 'any' types
- transfer base interfaces (recipe, recipe actions) to separate layer ( => clean architecture)
- add TSLint

