import { IAppState, IRecipeState, AnyRecipeActions, UPSERT_RECIPES } from './types';

const initialState: IAppState = {
  recipes: {},
};

function appReducer(state = initialState, action: AnyRecipeActions): IAppState {
  if (action.type === UPSERT_RECIPES) {
    const set: IRecipeState = {};

    action.payload.forEach(item => {
      set[item.id] = item;
    });

    return Object.assign({}, state, {
      recipes: {
        ...state.recipes,
        ...set,
      },
    });
  }

  return state;
}

export default appReducer;
