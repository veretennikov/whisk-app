export const UPSERT_RECIPES: string = 'UPSERT_RECIPES';

// entities
export interface IRecipeEntity {
  id: string,
  name: string,
  image: string,
  ingredients: string[],
  instructions: string[],
}

// states
export interface IRecipeState {
  [key: string]: IRecipeEntity,
}

export interface IAppState {
  recipes: IRecipeState,
}

// actions
export interface IUpsertRecipesAction {
  type: typeof UPSERT_RECIPES,
  payload: IRecipeEntity[],
}

export type AnyRecipeActions = IUpsertRecipesAction;
