import { UPSERT_RECIPES, IRecipeEntity, IUpsertRecipesAction } from './types';

export function upsertRecipe(payload: IRecipeEntity[]): IUpsertRecipesAction {
  return {
    type: UPSERT_RECIPES,
    payload,
  }
}

