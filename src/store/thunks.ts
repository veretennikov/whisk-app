import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { IAppState } from './types';
import dataProvider from '../services/api-provider';
import { upsertRecipe } from './actions';

export function loadRecipes(): ThunkAction<void, IAppState, null, Action<string>> {
  return function (dispatch) {
    dataProvider.getRecipes()
      .then((data) => {
        dispatch(upsertRecipe(data));
      })
      .catch((err) => {
        console.log('Connection error');
      });
  };
}

export function loadRecipeById(id: string): ThunkAction<void, IAppState, null, Action<string>> {
  return function thunk(dispatch) {
    dataProvider.getRecipeById(id)
      .then((data) => {
        dispatch(upsertRecipe([data]));
      })
      .catch((err) => {
        console.log('Connection error');
      });
  };
}
