import * as needle from 'needle';

import config from '../config';
import { IRecipeEntity } from '../store/types';
import { IRecipeItemFromAPI } from './api-types';

class ApiProvider {
  apiUrl: string;

  cursor: string = '';

  requestOptions: needle.NeedleOptions;

  constructor(apiUrl: string) {
    this.apiUrl = apiUrl;
    this.requestOptions = {
      follow_max: 2, // without it needle will refuse redirect from proxy
    };
  }

  static normalizeRecipe(item: IRecipeItemFromAPI): IRecipeEntity {
    return {
      id: item.id,
      name: item.name,
      image: item.images[0].url,
      ingredients: item.ingredients.map(ingredient => ingredient.text),
      instructions: item.instructions !== undefined ? item.instructions.steps.map(instruction => instruction.text) : [],
    };
  }

  getRecipes(): Promise<IRecipeEntity[]> {
    return new Promise((resolve, reject) => {
      let url:string = `${this.apiUrl}/v1/search?type=recipe`;

      if (!!this.cursor && this.cursor.length > 0) {
        url = `${url}&after=${this.cursor}`;
      }

      needle.get(url, this.requestOptions, (err, res) => {
        if (err) {
          reject(err);
          return;
        }

        this.cursor = res.body.paging.cursors.after;

        const data: IRecipeEntity[] = [];

        res.body.data.forEach((item:any) => {
          const recipe: any = item.content;
          data.push(ApiProvider.normalizeRecipe(recipe));
        });

        resolve(data);
      });
    });
  }

  getRecipeById(id: string): Promise<IRecipeEntity> {
    return new Promise((resolve, reject) => {
      const url: string = `${this.apiUrl}/v1/${id}?fields=instructions`;

      needle.get(url, this.requestOptions, (err, res) => {
        if (err) {
          reject(err);
          return;
        }

        const data: IRecipeEntity = ApiProvider.normalizeRecipe(res.body);

        resolve(data);
      });
    });
  }
}

export default new ApiProvider(config.api);
