export interface IRecipeItemFromAPI {
  id: string,
    name: string,
    images: any[],
    ingredients: [{
    text: string,
  }],
    instructions?: {
      steps: [{
        text: string,
      }]
    },
}
