const proxyUrl: string = 'http://localhost' || process.env.PROXY_PATH;
const proxyPort: number = 8888 || process.env.PROXY_PORT;

const api: string = `${proxyUrl}:${proxyPort}`;

export default {
  api,
};
