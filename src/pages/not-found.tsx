import * as React from 'react';
import { Link } from 'react-router-dom';

import BaseLayout from '../layouts/base';
import Button from '../components/button';

const NotFound: React.FC = function() {
  return (
    <BaseLayout>
      <h1>Page not found</h1>
      <Link to="/">
        <Button>TO HOME</Button>
      </Link>
    </BaseLayout>
  );
};

export default NotFound;
