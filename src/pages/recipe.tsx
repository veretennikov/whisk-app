import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { IAppState, IRecipeEntity } from '../store/types';
import { loadRecipeById } from '../store/thunks';
import BaseLayout from '../layouts/base';
import Button from '../components/button';
import Spinner from '../components/spinner';
import RecipePicture from '../components/recipe-picture';
import RecipeDetails from '../components/recipe-details';

interface IRecipePage {
  recipe: IRecipeEntity,
  match: {
    params: {
      id: string,
    },
  },
  loadById: (id: string) => void,
}

class RecipePage extends React.Component<IRecipePage> {
  componentDidMount() {
    this.props.loadById(this.props.match.params.id);
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <BaseLayout>
        {Object.keys(this.props.recipe).length === 0 ? (
          <Spinner />
        ) : (
          <React.Fragment>
            <h1>{this.props.recipe.name}</h1>

            <RecipePicture image={this.props.recipe.image} />

            <RecipeDetails title="Ingredients" items={this.props.recipe.ingredients} />

            {this.props.recipe.instructions.length > 0 ? (
              <RecipeDetails title="Instructions" items={this.props.recipe.instructions} />
            ) : (
              <Spinner text="loading instructions..." />
            )}
          </React.Fragment>
        )}

        <Link to="/">
          <Button>TO HOME</Button>
        </Link>
      </BaseLayout>
    );
  }
}

function mapStateToProps(state: IAppState, ownProps: any): { recipe: IRecipeEntity } {
  return {
    recipe: state.recipes[ownProps.match.params.id] || {},
  };
}

const mapDispatchToProps = {
  loadById: loadRecipeById,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RecipePage));
