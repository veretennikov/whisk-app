import React from 'react';
import { connect } from 'react-redux';

import { IAppState, IRecipeState } from '../store/types';
import { loadRecipes } from '../store/thunks';
import BaseLayout from '../layouts/base';
import Button from '../components/button';
import RecipeList from '../components/recipe-list';
import Spinner from '../components/spinner';

interface IMainPageProps {
  recipes: IRecipeState,
  load: () => void,
}

class MainPage extends React.Component<IMainPageProps, {}> {
  constructor(props: IMainPageProps) {
    super(props);
    this.handleLoad = this.handleLoad.bind(this);
  }

  componentDidMount() {
    if (Object.keys(this.props.recipes).length <= 1) {
      this.props.load();
    }
  }

  handleLoad() {
    this.props.load();
  }

  render() {
    const { recipes } = this.props;

    return (
      <BaseLayout>
        <h1>Delicious feed</h1>

        {Object.keys(recipes).length <= 1 ? (
          <Spinner />
        ) : (
          <React.Fragment>
            <RecipeList recipes={recipes} />
            <Button onClick={this.handleLoad} type="submit">LOAD</Button>
          </React.Fragment>
        )}
      </BaseLayout>
    );
  }
}

function mapStateToProps(state: IAppState): { recipes: IRecipeState } {
  return {
    recipes: state.recipes,
  };
}

const mapDispatchToProps = {
  load: loadRecipes,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
