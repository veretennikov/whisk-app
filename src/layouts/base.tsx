import React from 'react';

import Header from '../components/header';
import Logo from '../components/logo';

import './base.css';

const BaseLayout: React.FC = function(props) {
  return (
    <div className="BaseLayout">
      <div className="BaseLayout-Header">
        <Header>
          <Logo />
        </Header>
      </div>

      <div className="BaseLayout-Content">
        {props.children}
      </div>
    </div>
  );
}

export default BaseLayout;
