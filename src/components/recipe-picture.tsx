import React from 'react';

import './recipe-picture.css';

interface IRecipePictureProps {
  image: string,
}

const RecipePicture: React.FC<IRecipePictureProps> = function(props) {
  return (
    <div className="RecipePicture">
      <img className="RecipePicture-Image" src={props.image} alt="" />
    </div>
  );
};

export default RecipePicture;
