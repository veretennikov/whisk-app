import React from 'react';

import './header.css';

const Header: React.FC = function(props) {
  return (
    <div className="Header">
      {props.children}
    </div>
  );
};

export default Header;
