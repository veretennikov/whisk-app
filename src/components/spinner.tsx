import React from 'react';

import './spinner.css';

interface ISpinnerProps {
  text?: string,
}

const Spinner: React.FC<ISpinnerProps> = function(props) {
  const { text } = props;
  return (
    <div className="Spinner">{text !== undefined ? props.text : 'loading...'}</div>
  );
};

export default Spinner;
