import React from 'react';

import RecipeBox from './recipe-box';

import './recipe-list.css';

interface IRecipeListProps {
  recipes: any,
}

const RecipeList: React.FC<IRecipeListProps> = function(props) {
  return (
    <ul className="RecipeList">
      {
        Object.keys(props.recipes).map(key => (
          <li className="RecipeList-Item" key={key}>
            <RecipeBox
              link={`/recipes/${props.recipes[key].id}`}
              image={props.recipes[key].image}
              title={props.recipes[key].name}
            />
          </li>
        ))
      }
    </ul>
  );
};

export default RecipeList;
