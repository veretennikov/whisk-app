import React from 'react';
import { Link } from 'react-router-dom';

import './recipe-box.css';

interface IRecipeBoxProps {
  link: string,
  image: string,
  title: string,
}

const RecipeBox: React.FC<IRecipeBoxProps> = function(props) {
  return (
    <Link className="RecipeBox" to={props.link}>
      <img className="RecipeBox-Image" src={props.image} alt="" />
      <span className="RecipeBox-Title">{props.title}</span>
    </Link>
  );
};

export default RecipeBox;
