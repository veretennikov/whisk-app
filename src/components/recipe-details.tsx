import React from 'react';

import './recipe-details.css';

interface IRecipeDetailsProps {
  title: string,
  items: string[],
}

const RecipeDetails: React.FC<IRecipeDetailsProps> = function(props) {
  return (
    <div className="RecipeDetails">
      <h2>{props.title}</h2>
      <ul className="RecipeDetails-List">
        {props.items.map(item => (
          <li className="RecipeDetails-Option" key={item}>
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default RecipeDetails;
