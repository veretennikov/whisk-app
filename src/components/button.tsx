import React from 'react';

import './button.css';

interface IButtonProps {
  type?: string,
  onClick?: () => void
}


const Button: React.FC<IButtonProps> = function(props) {
  return (
    <button className="Button" {...props}>{props.children}</button>
  );
};

export default Button;
