import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from '../pages/home';
import RecipePage from '../pages/recipe';
import NotFound from '../pages/not-found';

class Routes extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/recipes/:id" component={RecipePage} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default Routes;
