/**
 * Proxy server for developing
 *
 * WARNING! Not for production!
 */
const express = require('express');
const proxy = require('express-http-proxy');
const cors = require('cors');

const app = express();

const config = {
  url: 'https://graph.whisk.com/' || process.env.PROXY_URL,
  token: process.env.PROXY_TOKEN,
  port: 8888 || process.env.PROXY_PORT,
};

app.use(cors());

app.use('/', proxy(config.url, {
  proxyReqOptDecorator(proxyReqOpts) {
    proxyReqOpts.headers['Content-Type'] = 'application/json';
    proxyReqOpts.headers['Accept'] = 'application/json';
    proxyReqOpts.headers['Authorization'] = `Bearer ${config.token}`;

    return proxyReqOpts;
  }
}));

if (process.env.NODE_ENV === 'development') {
  app.listen(config.port, () => {
    console.log(`Proxy server have been started on port ${config.port}`);
  });
} else {
  console.log('Proxy server is only for development');
}

